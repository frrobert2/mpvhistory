-- Music Log for MPV player
--Rev. Fr. Robert Bower
--
--
--Bits and bobs taken from 
--********
-- -- Copyright (c) 2021, Eisa AlAwadhi
-- License: BSD 2-Clause License

-- Creator: Eisa AlAwadhi
-- Project: SmartHistory
-- Version: 1.7.1
-- *************


mp.register_event('file-loaded', function()
	filePath = mp.get_property('path')
        metadata = mp.get_property('filtered-metadata')
	end)
	
mp.register_event('file-loaded', function()
	empty = false
	local historyLog = (os.getenv('APPDATA') or os.getenv('HOME')..'/.config')..'/mpv/mpvHistory.log'
	local historyLogAdd = io.open(historyLog, 'a+')
	
	
	if (filePath ~= nil) then
		historyLogAdd:write(('[%s] %s\n'):format(os.date('%d/%b/%y %X'), metadata ))
		historyLogAdd:close()
	end
end)


